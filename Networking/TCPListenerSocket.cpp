// Fill out your copyright notice in the Description page of Project Settings.

#include "include/TCPListenerSocket.h"

TCPListenerSocket::TCPListenerSocket(const uint32 IP, const uint32 Port, const FString& SocketName, const int32 ReceiveBufferSize)
	: TCPSocket(IP, Port, ReceiveBufferSize), _SocketName(SocketName)
{
	CreateListenerSocket();
}

TCPListenerSocket::TCPListenerSocket(const FString& HostName, const uint32 Port, const FString& SocketName, const int32 ReceiveBufferSize)
	: TCPSocket(HostName, Port, ReceiveBufferSize), _SocketName(SocketName)
{
	this->_HostNameFunctor.BindRaw(this, &TCPListenerSocket::HostNameCallback);
	_LookupTask = new FAutoDeleteAsyncTask<UHostNameTask>(_HostNameFunctor, _HostName);
	_LookupTask->StartBackgroundTask();
}

TCPListenerSocket::~TCPListenerSocket()
{
	if (_ListenerSocket != NULL) {
		_ListenerSocket->Close();
		delete _ListenerSocket;
		_ListenerSocket = NULL;
	}
}

void TCPListenerSocket::HostNameCallback(FString IP)
{
	TCPSocket::HostNameCallback(IP);
	CreateListenerSocket();
}

void TCPListenerSocket::CreateListenerSocket()
{
	//Create the socket
	if (!_ListenerSocket)
	{
		_ListenerSocket = CreateTCPConnectionListener();
	}

	//Not created?
	if (!_ListenerSocket)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("CreateListenerSocket>> Listener socket could not be created on IP: %d and Port: %d"), _IP, _Port));
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("CreateListenerSocket>> Listener Created on IP: %u and Port: %d"), _IP, _Port));
	}
}

FSocket* TCPListenerSocket::CreateTCPConnectionListener()
{
	FIPv4Endpoint Endpoint(FIPv4Address(_IP), _Port);
	FSocket* ListenSocket = NULL;
	ListenSocket = FTcpSocketBuilder(_SocketName)
		.AsReusable()
		.BoundToEndpoint(Endpoint)
		.Listening(8);

	//Set Buffer Size
	int32 NewSize = 0;
	if (ListenSocket)
	{
		ListenSocket->SetReceiveBufferSize(_ReceiveBufferSize, NewSize);
		ListenSocket->SetLinger(false, 0);
	}

	//Done!
	return ListenSocket;
}

//TCP Connection Listener
bool TCPListenerSocket::Connect()
{
	if(!_ListenerSocket)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Connect>> Listener socket not created on IP: %u and Port: %d"), _IP, _Port));
		return false;
	}

	//Remote address
	TSharedRef<FInternetAddr> RemoteAddress = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	bool Pending;

	// handle incoming connections
	_ListenerSocket->HasPendingConnection(Pending);

	if (Pending)
	{
		//Already have a Connection? destroy previous
		if (_RemoteSocket != NULL)
		{
			_RemoteSocket->Close();
			ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->DestroySocket(_RemoteSocket);
		}

		//New Connection receive!
		_RemoteSocket = _ListenerSocket->Accept(*RemoteAddress, TEXT("[Fireworks] Unreal Engine Remote Socket"));

		if(_RemoteSocket)
		{
			//Global cache of current Remote Address
			_RemoteAddressForConnection = FIPv4Endpoint(RemoteAddress);
			_RemoteSocket->SetLinger(false, 0);
			_FirstRead = true;
		}
	}
	return true;
}

void TCPListenerSocket::Close()
{	
	TCPSocket::Close();
}
