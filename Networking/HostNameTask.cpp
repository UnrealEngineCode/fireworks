// Fill out your copyright notice in the Description page of Project Settings.

#include "include/HostNameTask.h"
#include "CoreMinimal.h"
#include "Networking.h"
#include "Engine.h"

UHostNameTask::UHostNameTask(const FHostnameLookup CB, const FString& HName)
	: Callback(CB), HostName(HName)
{
	IPaddr = NULL;
}

UHostNameTask::UHostNameTask(uint32* IP, const FString& HName)
	: IPaddr(IP), HostName(HName)
{
	Callback = NULL;
}

UHostNameTask::UHostNameTask()
{
}

UHostNameTask::~UHostNameTask()
{
}

void UHostNameTask::DoWork()
{
	uint32 IP = LookupHostName(HostName);
	FString IPString = FString::Printf(TEXT("%u"), IP);
	if(Callback.IsBound()) {
		Callback.ExecuteIfBound(IPString);
	}
	if (IPaddr != nullptr) {
		*IPaddr = IP;
	}
}

uint32 UHostNameTask::LookupHostName(const FString HostName)
{
	uint32 OutIP = 0;
	int32 ErrorCode = 10;

	ISocketSubsystem* const SocketSubSystem = ISocketSubsystem::Get();

	if (SocketSubSystem)
	{

		auto ResolveInfo = SocketSubSystem->GetHostByName(TCHAR_TO_ANSI(*HostName));

		while (!ResolveInfo->IsComplete()) {
			FPlatformProcess::Sleep(0.1);
		}
		ErrorCode = ResolveInfo->GetErrorCode();
		if (ErrorCode == 0)
		{
			const FInternetAddr* Addr = &ResolveInfo->GetResolvedAddress();
			Addr->GetIp(OutIP);
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("WAZAAAAAAPPPP is %d.%d.%d.%d: "), 0xff & (OutIP >> 24), 0xff & (OutIP >> 16), 0xff & (OutIP >> 8), 0xff & OutIP));
		}
	}
	return OutIP;
}
