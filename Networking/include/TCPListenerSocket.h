// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TCPSocket.h"


class TCPSocket;

/**
 * 
 */
class MYPROJECT_API TCPListenerSocket : public TCPSocket	
{

protected:
	FSocket* _ListenerSocket = NULL;
	FString _SocketName;

	FSocket* CreateTCPConnectionListener();
	void HostNameCallback(FString IP);
	void CreateListenerSocket();

public:
	TCPListenerSocket(const uint32 IP, const uint32 Port, const FString& SocketName, const int32 ReceiveBufferSize = 2 * 1024 * 1024);
	TCPListenerSocket(const FString& HostName, uint32 Port, const FString& SocketName, const int32 ReceiveBufferSize = 2 * 1024 * 1024);

	~TCPListenerSocket();

	bool Connect();

	void Close();

};
