// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "AsyncWork.h"
//#include "HostNameTask.generated.h"

/**
 * 
 */

//UCLASS()
class UHostNameTask : public FNonAbandonableTask
{
	//GENERATED_BODY()
	friend class FAutoDeleteAsyncTask<UHostNameTask>;

public:

	DECLARE_DELEGATE_OneParam(FHostnameLookup, FString);

	UHostNameTask(const FHostnameLookup CB, const FString& HName);
	UHostNameTask(uint32* IP, const FString& HName);
	UHostNameTask();
	~UHostNameTask();

protected:
	static uint32 LookupHostName(const FString HostName);

	FHostnameLookup Callback;
	FString HostName;
	uint32* IPaddr;
	void DoWork();

	// This next section of code needs to be here.  Not important as to why.

	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FMyTaskName, STATGROUP_ThreadPoolAsyncTasks);
	}
};

