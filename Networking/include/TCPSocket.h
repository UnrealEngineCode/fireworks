// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Networking.h"
#include "HostNameTask.h"
#include "CoreMinimal.h"

/**
 * 
 */

DECLARE_DELEGATE_OneParam(FHostnameLookup, FString);

class MYPROJECT_API TCPSocket
{
protected:

	FString _HostName;
	uint32 _IP;
	uint32 _Port;
	int32 _ReceiveBufferSize;

	FSocket* _RemoteSocket = NULL;
	FIPv4Endpoint _RemoteAddressForConnection;

	FHostnameLookup _HostNameFunctor;
	FAutoDeleteAsyncTask<UHostNameTask>* _LookupTask = NULL;

	bool _FirstRead = true;
	double _DeltaReadTimeout = 0;
	double _DeltaWriteTimeout = 0;

public:

	TCPSocket(const uint32 IP, const uint32 Port, const int32 ReceiveBufferSize = 2 * 1024 * 1024);
	TCPSocket(const FString& HostName, uint32 Port, const int32 ReceiveBufferSize = 2 * 1024 * 1024);
	virtual ~TCPSocket();

	FString StringFromBinaryArray(TArray<uint8> BinaryArray);

	void HostNameCallback(FString IP);

	FSocket* ConnectTCPSocket();

	virtual bool Connect();

	virtual bool CheckConnectionExists(double& DeltaTime, double Timeout = -1.0);

	virtual bool CheckConnectionExists();

	virtual void Close();

	virtual void Read(FString& RecievedString, const double Timeout = 10.0);

	virtual void Read(TArray<uint8>& ReceivedData, uint32 BytesToRead, const double Timeout = 10.0);

	virtual void Write(FString StringToSend);

	virtual void Write(TArray<uint8>& DataToSend);
};
