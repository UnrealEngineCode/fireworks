// Fill out your copyright notice in the Description page of Project Settings.

#include "include/TCPSocket.h"
#include <string>

TCPSocket::TCPSocket(uint32 IP, uint32 Port, const int32 ReceiveBufferSize):
	_IP(IP), _Port(Port), _ReceiveBufferSize(ReceiveBufferSize)
{
	_HostName.Empty();
}

TCPSocket::TCPSocket(const FString& HostName, uint32 Port, const int32 ReceiveBufferSize) :
	_HostName(HostName), _Port(Port), _ReceiveBufferSize(ReceiveBufferSize)
{
	_IP = 0;
}

void TCPSocket::HostNameCallback(FString IP) 
{
	uint32 IPnbr = (uint32)FCString::Strtoui64(*IP, NULL, 10);
	_IP = IPnbr;
}

TCPSocket::~TCPSocket()
{
	Close();
}

bool TCPSocket::Connect()
{
	// Calling Connect() before lookup is complete will fail
	if (!_HostName.IsEmpty())
	{
		return _IP != 0;
	}

	if (!_RemoteSocket)
	{
		_RemoteSocket = ConnectTCPSocket();
	}
	return CheckConnectionExists();
}

void TCPSocket::Close()
{
	if (_RemoteSocket != NULL) {
		_RemoteSocket->Close();
		delete _RemoteSocket;
		_RemoteSocket = NULL;
	}
	_FirstRead = true;
}

bool TCPSocket::CheckConnectionExists(double& DeltaTime, const double Timeout)
{
	int32 sent = 0;
	bool ConnectionStatus = false;
	if (_RemoteSocket)
	{
		ConnectionStatus =
			_RemoteSocket->GetConnectionState() == ESocketConnectionState::SCS_Connected
			&& _RemoteSocket->Send(NULL, 0, sent);

		if (!ConnectionStatus || ((Timeout >= 0.0) && (FPlatformTime::Seconds() - DeltaTime >= Timeout)))
		{
			DeltaTime = FPlatformTime::Seconds();
			Close();
		}
	}
	return ConnectionStatus;
}

bool TCPSocket::CheckConnectionExists()
{
	double DeltaTime = 0;
	return CheckConnectionExists(DeltaTime, -1.0);
}

void TCPSocket::Read(TArray<uint8>& ReceivedData, uint32 BytesToRead, const double Timeout)
{
	const uint8 Zero = 0;
	uint32 Size;
	if (_FirstRead)
	{
		_DeltaReadTimeout = FPlatformTime::Seconds();
		_FirstRead = false;
	}

	while (_RemoteSocket && _RemoteSocket->HasPendingData(Size))
	{
		if (Size >= BytesToRead)
		{
			ReceivedData.Init(Zero, BytesToRead);
			int32 Read = 0;
			bool successful = _RemoteSocket->Recv(ReceivedData.GetData(), ReceivedData.Num(), Read, ESocketReceiveFlags::None);
			if (!successful)
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Read>> Failed Recieving Message")));
			}
			if (ReceivedData.Num() <= 0 || !successful)
			{
				CheckConnectionExists();
			}
			else
			{
				_DeltaReadTimeout = FPlatformTime::Seconds();
				return;
			}
		}
	}
	if (Timeout >= 0 && _RemoteSocket && !_RemoteSocket->HasPendingData(Size))
	{
		CheckConnectionExists(_DeltaReadTimeout, Timeout);
	}
}

void TCPSocket::Read(FString& RecievedString, const double timeout)
{
	TArray<uint8> ReceivedData;
	ReceivedData.Empty();
	int32 len = 0;

	//read first bytes that define the string length
	Read(ReceivedData, sizeof(int32));

	if (ReceivedData.Num() == sizeof(int32))
	{
		FMemory::Memcpy(&len, &ReceivedData.GetData()[0], sizeof(int32));
		ReceivedData.Empty();
		Read(ReceivedData, len);
	}
	if (ReceivedData.Num() == len)
	{
		const FString ReceivedUE4String = StringFromBinaryArray(ReceivedData);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Emerald, ReceivedUE4String);
		RecievedString = ReceivedUE4String;
	}
}

void TCPSocket::Write(TArray<uint8>& DataToSend)
{
	int32 sent = 0;
	if (_RemoteSocket)
	{
		if (_RemoteSocket->Send(NULL, 0, sent)
			&& _RemoteSocket->GetConnectionState() == ESocketConnectionState::SCS_Connected)
		{
			bool successful = _RemoteSocket->Send(DataToSend.GetData(), DataToSend.Num(), sent);
			if (!successful)
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Write>> Failed Sending Message ~> %s"), &DataToSend));
				CheckConnectionExists();
			}
		}
		else
		{
			CheckConnectionExists();
		}
	}
}

void TCPSocket::Write(FString StringToSend)
{
	uint8 size[sizeof(int32)];
	TCHAR *SerializedChar = StringToSend.GetCharArray().GetData();
	int32 Size = FCString::Strlen(SerializedChar);
	FMemory::Memcpy(size, &Size, sizeof(int32));
	int32 Sent = 0;
	uint8* ResultChars = (uint8*)TCHAR_TO_UTF8(SerializedChar);
	TArray<uint8> DataToSend;
	DataToSend.Empty();
	DataToSend.AddUninitialized(Size + sizeof(int32));
	FMemory::Memcpy(DataToSend.GetData(), size, sizeof(int32));
	FMemory::Memcpy(DataToSend.GetData() + sizeof(int32), ResultChars, Size*sizeof(uint8));
	Write(DataToSend);
}

FSocket* TCPSocket::ConnectTCPSocket()
{
	FIPv4Address ip(_IP);
	TSharedRef<FInternetAddr> addr = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	addr->SetIp(ip.Value);
	addr->SetPort(_Port);
	FSocket* socket = NULL;
	socket = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateSocket(NAME_Stream, TEXT("TCPSocket>> Unreal Socket"), false);
	bool connected = false;
	if (socket)
	{
		bool connected = socket->Connect(*addr);
		if(connected) {
			_FirstRead = true;
			socket->SetLinger(false,0);
		}
		else
		{
			socket->Close();
			delete socket;
			socket = NULL;
		}
	}
	return socket;
}

FString TCPSocket::StringFromBinaryArray(TArray<uint8> BinaryArray)
{
	//Create a string from a byte array!
	const std::string cstr(reinterpret_cast<const char*>(BinaryArray.GetData()), BinaryArray.Num());
	return FString(cstr.c_str());
}
